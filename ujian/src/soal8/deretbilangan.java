package soal8;

import java.util.Scanner;

public class deretbilangan {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Masukan panjang deret bilangan");
        int panjang = scan.nextInt();
        
        
        int hitung=2;
        int angka =5;
        
        int[] prima = new int[panjang];
        prima[0]=2;
        prima[1]=3;
        luar:
        for(;hitung<panjang;angka+=2) {
        	long limit = (long)Math.ceil(Math.sqrt((double) angka));
        	for(int i=1;i<hitung && prima[i]<=limit;i++) {
        		if(angka%prima[i]==0) {
        			continue luar;
        		}
        	}prima[hitung++]=angka;
        	
        }
        for(int i=0;i<prima.length;i++) {
        	System.out.print(prima[i]+ " ");
        }
        
        System.out.println();
        int[] fib = new int[panjang];
        fib[0]=0;
        fib[1]=1;
        
        for(int i=2;i<panjang;i++) {
        	fib[i] = fib[i-1] + fib[i-2];
        }
        for(int i=0;i<panjang;i++) {
        	System.out.print(fib[i]+" ");
        }
        
        System.out.println();
        int[] hasil = new int[panjang];
        for(int i=0;i<panjang;i++) {
        	hasil[i]=prima[i]+fib[i];
        }
        for(int i=0;i<panjang;i++) {
        	System.out.print(hasil[i]+" ");
        }
        
        
        scan.close();
    }
}
