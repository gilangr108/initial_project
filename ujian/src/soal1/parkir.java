package soal1;

import java.util.Scanner;

public class parkir {
    protected static Scanner input;

    public static void main(String[] args) {
    	Scanner inputan = new Scanner(System.in);

        System.out.println("Tanggal Masuk : " );
        String tanggalMasuk = inputan.nextLine();

        System.out.println("Tanggal Keluar : ");
        String tanggalKeluar = inputan.nextLine();


        System.out.println("Jam Masuk : ");
        String jamMasuk = inputan.nextLine();

        System.out.println("Jam Keluar : ");
        String jamKeluar = inputan.nextLine();

        String tglMsk = tanggalMasuk.substring(0,2);
        String tglKlr = tanggalKeluar.substring(0,2);
        String jmMsk = jamMasuk.substring(0,2);
        String jmKlr = jamKeluar.substring(0,2);

        int tglMskint = Integer.parseInt(tglMsk);
        int tglKlrint = Integer.parseInt(tglKlr);
        int jmMskint = Integer.parseInt(jmMsk);
        int jmKlrint = Integer.parseInt(jmKlr);

        int lamaparkir = tglKlrint - tglMskint;
        int lamajamparkir = jmKlrint - jmMskint;
        int lamaparkirtojam = (lamaparkir*24);
        int totallamaparkir = lamaparkirtojam+lamajamparkir;

        int denda =0;
        int dendaperhari = 0;
        int dendamaxhari =0;
        int totaldenda = 0;
        denda=(1000*lamajamparkir);
        dendaperhari = 15000;
        dendamaxhari = 8000;


        while (true)
        if (totallamaparkir>=24) {
            totallamaparkir = totallamaparkir - 24;
            totaldenda = totaldenda+dendaperhari;
        } else if (totallamaparkir >=8 && totallamaparkir <24) {
                totaldenda = totaldenda+dendamaxhari;
                break;
        } else if (totallamaparkir<8 && totallamaparkir>0) {
            totaldenda = totaldenda + denda;
            break;
        }
        System.out.println("Lama parkir : " + lamaparkir + " hari " + lamajamparkir + " jam");
        System.out.println("Totaldenda = "+totaldenda);
    }
}
