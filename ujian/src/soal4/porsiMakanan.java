package soal4;

import java.util.Scanner;

public class porsiMakanan {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Laki-laki dewasa: ");
		int lkDewasa = input.nextInt();
		System.out.println("Perempuan dewasa: ");
		int pDewasa = input.nextInt();
		System.out.println("Balita: ");
		int balita = input.nextInt();
		System.out.println("Remaja: ");
		int remaja = input.nextInt();
		System.out.println("Anak-anak: ");
		int anak = input.nextInt();

		int mlkDewasa = 2;
		int mpDewasa = 1;
		int mbalita = 1;
		int mremaja = 2;
		double manak = 0.5;
		
		double total=0;
		
		total = (lkDewasa*mlkDewasa) + (pDewasa*mpDewasa) + (balita*mbalita) + (remaja*mremaja) + (anak*manak);
		System.out.println("Total porsi makanan yang dimakan adalah "+total+" porsi");
	}

}
